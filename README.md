# SNAKEAI

SnakeAI is a project to resolve snake game with Artificial intelligence.

This project has been coded with .NetCore.

## How does It work

This project use is based on DeepLearning (neural network) to resolve the game. The neural network is mutated with genetic algorithm (mutation & crossover) to increase the final score.

## Usage

You can use this project to test your own AI. 

This project has generic class function. You can implement multiple AI type in the same time.


See JLAIManager and JLAiNetwork for example. To test your own AI create two class:


```
public class MyCustomAIManager: ScorableNetworkBase

public class MyCustomNetwork: INetwork, IScorable

```

![](./snakegif.gif)

## License
[MIT](https://choosealicense.com/licenses/mit/)