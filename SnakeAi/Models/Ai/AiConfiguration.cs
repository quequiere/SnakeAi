﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnakeAi.Models.Ai
{
    public class AiConfiguration
    {
        public int hiddenNeuron { get; set; } = 18;
        public int hiddenNeuronLayer { get; set; } = 3;

        public int parentStartNumber { get; set; } = 50;
        public int parentMemoryNumber { get; set; } = 10;

        public int childerPerGeneration { get; set; } = 2000;
        public int generationNumber { get; set; } = 99999;

        public double mutationChance { get; set; } = 0.05;
        [Obsolete]
        public double crossOverChance { get; set; } = 0.5;

        public int parallelGames { get; set; } = 10;
    }
}
