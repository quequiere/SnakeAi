﻿using System;
using System.Collections.Generic;
using System.Text;
using SnakeAi.Models.SnakeModels;
using SnakeAi.Models.SnakeModels.Helpers;

namespace SnakeAi.Models.Ai.Visions
{
    public class FullVision : IVision
    {
        public double[] generateVision(Game game)
        {
            List<double> gameState = new List<double>();

            for(int x = 0; x< GameConfiguration.GetGameConfiguration().gameSize; x++)
            {
                for(int y = 0; y< GameConfiguration.GetGameConfiguration().gameSize; y++)
                {
                    var currentLocation = (x, y);
                    var apple = game.physicHelper.isCollideApple(currentLocation);
                    var snake = game.physicHelper.isCollideSnake(currentLocation);

                    if (apple)
                        gameState.Add(1);
                    else if (snake)
                        gameState.Add(2);
                    else
                        gameState.Add(0);


                }
            }

            gameState.Add(game.snake.getLocation().xPos);
            gameState.Add(game.snake.getLocation().yPos);

            return gameState.ToArray();
        }
    }
}
