﻿using SnakeAi.Models.SnakeModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SnakeAi.Models.Ai
{
    public interface IVision
    {
        double[] generateVision(Game game);
    }
}
