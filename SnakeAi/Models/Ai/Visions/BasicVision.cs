﻿using System;
using System.Collections.Generic;
using System.Text;
using SnakeAi.Models.SnakeModels;

namespace SnakeAi.Models.Ai.Visions
{
    public class BasicVision : IVision
    {
        public double[] generateVision(Game game)
        {
            var sensor1 = generateSensor(game,(0, 1));
            var sensor2 = generateSensor(game, (1, 1));
            var sensor3 = generateSensor(game, (1, 0));
            var sensor4 = generateSensor(game, (1, -1));
            var sensor5 = generateSensor(game, (0, -1));
            var sensor6 = generateSensor(game, (-1, -1));
            var sensor7 = generateSensor(game, (-1, 0));
            var sensor8 = generateSensor(game, (-1, 1));

            return new double[]
            {
                sensor1[0],
                sensor1[1],
                sensor1[2],

                sensor2[0],
                sensor2[1],
                sensor2[2],

                sensor3[0],
                sensor3[1],
                sensor3[2],

                sensor4[0],
                sensor4[1],
                sensor4[2],

                sensor5[0],
                sensor5[1],
                sensor5[2],

                sensor6[0],
                sensor6[1],
                sensor6[2],

                sensor7[0],
                sensor7[1],
                sensor7[2],

                sensor8[0],
                sensor8[1],
                sensor8[2],
            };
        }



        private double[] generateSensor(Game game,(int x, int y) direction)
        {
            double[] sensor = new double[3];
            double distanceToTheWall = 0.0;

            (int x, int y) currentScan = (game.snake.getLocation().xPos, game.snake.getLocation().yPos); ;

            do
            {
                distanceToTheWall++;
                currentScan = (currentScan.x + direction.x, currentScan.y + direction.y);

                if (game.physicHelper.isCollideApple(currentScan))
                    sensor[1] = 1;

                if (game.physicHelper.isCollideSnake(currentScan))
                    sensor[2] = 1;


            } while (!game.physicHelper.isCollideWall(currentScan));

            sensor[0] = 1.0 / distanceToTheWall;

            return sensor;

        }
    }
}
