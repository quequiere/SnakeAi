﻿using SnakeAi.Models.SnakeModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;

namespace SnakeAi.Models.Ai
{
    public interface INetwork
    {
        void applyMutation(double chance);

        void applyCrossOver(INetwork partner, double chance);

        INetwork copyNetwork();

        double[] computeNetwork(double[] inputs);
        double[] generateVision(Game game);
    }

    public interface IScorable : IComparable<IScorable>
    {
        double getFinalScore();
        void fixFinalScore(Game game);
    }

    public abstract class ScorableNetworkBase
    {

        public abstract IScorable GetScorable();
        public abstract INetwork GetNetwork();

        public abstract INetwork initializedNetwork(INetwork network);
        public abstract INetwork initializedRandomNetwork();

        public ScorableNetworkBase() {
            this.initializedRandomNetwork();
        }

        public ScorableNetworkBase(INetwork network)
        {
            this.initializedNetwork(network);
        }

        public void playGame(Game game, int iaSlow = 0, Action callBack = null)
        {

            if(game.gameIsLost)
            {
                throw new Exception("Game can't be fail before has been started !");
            }

            while(!game.gameIsLost)
            {
                var dir = getComputedDirection(game);
                if(iaSlow!=0)
                {
                    Task.Run(async delegate
                    {
                        await Task.Delay(iaSlow);
                    }).Wait();
                }
                game.fireAction(dir);

                if (callBack != null)
                    callBack.Invoke();
            }
        }

        private Direction getComputedDirection(Game game)
        {
            var vision = this.GetNetwork().generateVision(game);
            var output = this.GetNetwork().computeNetwork(vision);
            return convertArrayToDirection(output);
        }


        private Direction convertArrayToDirection(double[] outputs)
        {
            var temp = outputs.Select((output, index) => new { output = output, index = index })
              .OrderByDescending(res => res.output);

            var finded = temp.First().index;

            switch (finded)
            {
                case 0:
                    return Direction.west;
                case 1:
                    return Direction.est;
                case 2:
                    return Direction.south;
                case 3:
                    return Direction.north;
                default:
                    throw new Exception("Error while evaluate direction: " + finded);
            }
        }
    }



}
