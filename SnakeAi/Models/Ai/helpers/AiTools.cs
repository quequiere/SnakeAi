﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnakeAi.Models.Ai.helpers
{
    public class AiTools
    {
        public static double tryMixRandom(double chance, double firstValue, double secondValue)
        {
            Random rnd = new Random();
            double chanceChange = rnd.NextDouble();

            if (chanceChange <= chance)
            {
                return secondValue;
            }
            return firstValue;
        }

        public static double tryChangeRandom(double chance, double originalValue)
        {
            Random rnd = new Random();
            double chanceChange = rnd.NextDouble();

            if (chanceChange <= chance)
            {
                double value = rnd.NextDouble();
                if (rnd.NextDouble() < 0.5) value *= -1;
                return value;
            }
            return originalValue;
        }
    }
}
