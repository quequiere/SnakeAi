﻿using Newtonsoft.Json;
using SnakeAi.Models.Ai;
using SnakeAi.Models.SnakeModels;
using SnakeAi.Models.SnakeModels.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SnakeAi.Services
{
    public class AiManager<T> where T: ScorableNetworkBase , new()
    {
        public AiConfiguration config { get; }

        private List<T> bestParents { get; set; } = new List<T>();
        private int generationComputed = 0;

        private GameDrawerService drawer { get; }

        public bool replayGames { get; set; }

        public DisplayableData displayableData { get; set; } = new DisplayableData();

        private bool iaComputing { get; set; } = true;



        public AiManager(GameDrawerService drawer)
        {
            this.config = new AiConfiguration();
            this.drawer = drawer;
        }

        public void startAi(bool withReplay = false)
        {
            iaComputing = true;

            this.replayGames = withReplay;

            Task.Run(async delegate
            {
                await Task.Delay(1500);


                if (bestParents.Count < 1)
                {
                    Debug.WriteLine($"Starting SnakeAI with {config.parentStartNumber} random parents");
                    bestParents = Enumerable.Range(0, config.parentStartNumber).Select(_ => (new T())).ToList();
                }
                else
                    Debug.WriteLine("Parents was already loaded, start from them !");

                Enumerable.Range(0, config.generationNumber).ToList().ForEach(_ => { 

                    if(this.iaComputing)
                    {
                        this.playGeneration();
                        displayableData.currentGeneration++;
                        displayableData.totalNetworkCreated = displayableData.currentGeneration * config.childerPerGeneration;
                        displayableData.bestScore = this.bestParents.Select(p => p.GetScorable().getFinalScore()).OrderByDescending(t => t).First();
                    }

                });

            });

         
        }

        public bool loadNetwork(string fileNameLocation)
        {
            string readText = File.ReadAllText(fileNameLocation);
            var lines = JsonConvert.DeserializeObject<List<String>>(readText);

            //Usefull to access on deserialization functions
            T tempsNetwork = new T();
            
            if(! (tempsNetwork is ISavableNetwork))
            {
                throw new Exception("This type of network doesn't implement save / load functions");
            }

            ISavableNetwork tempsNetworkLoader = tempsNetwork as ISavableNetwork;

            var loaded = lines.Select(l => tempsNetworkLoader.stringToNetwork(l)).Select(n => this.createScorable(n)).ToList();
            this.bestParents = loaded;

            Debug.WriteLine(this.bestParents.Count + " parents loaded !");
            return true;
        }

        public bool saveNetwork(string fileNameLocation)
        {
            if (bestParents.All(p => p is ISavableNetwork))
            {
                var listToSave = bestParents.Select(p => p as ISavableNetwork).Select(p => p.networkToString()).ToList();
                var toWrite = JsonConvert.SerializeObject(listToSave);
                try
                {
                    File.WriteAllText(fileNameLocation, toWrite);
                    return true;
                }
                catch(Exception e)
                {
                    Debug.WriteLine("Fail to save file !");
                    Debug.WriteLine(e.StackTrace);
                    throw e;
                }
             
            }
            else
            {
                Debug.WriteLine("This network has no function implemented to be saved !");
            }
            return false;
                
        }

        private void playGeneration()
        {


            // =========================== GENERATION JOB ==================================

            Debug.WriteLine($"Starting generation {generationComputed++} with {config.childerPerGeneration} children");

            var startTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            var newtworks = generateGeneration(config.childerPerGeneration);

            var networkCreationTime = (DateTimeOffset.Now.ToUnixTimeMilliseconds() - startTime) / 1000.0;

            Debug.WriteLine($"Created {newtworks.Count} networks in {networkCreationTime} seconds");




            // =========================== GAME JOB ==================================



            runGames(newtworks);

            var gameCalculTime = (DateTimeOffset.Now.ToUnixTimeMilliseconds() - startTime) / 1000.0;
            Debug.WriteLine($"Games calculated in {gameCalculTime} seconds");



            // =========================== SCORE JOB ==================================

            //var t = newtworks
            //    .OrderByDescending(n => n.GetScorable()).ToList();

            var eliteGeneation =  newtworks
                .OrderByDescending(n => n.GetScorable())
                .Take(config.parentStartNumber)
                .ToList();

            bestParents.AddRange(eliteGeneation);

            bestParents = bestParents
                .OrderByDescending(n => n.GetScorable())
                .Take(config.parentMemoryNumber)
                .ToList();


            Debug.WriteLine("Parents updated:");
            bestParents.ForEach(p => {
                Debug.WriteLine($"- {p.GetScorable().getFinalScore()}");
            });


            // =========================== GAME JOB ==================================

            if(this.replayGames)
                this.replay();

        }

        public void replayOnDemand()
        {
            this.replayGames = true;
            this.replay();
        }

        public void stopAI()
        {
            this.iaComputing = false;
            Debug.WriteLine("AI will be stop ASAP");
        }

        private void replay()
        {
            Debug.WriteLine("Starting Replay:");
            bestParents.ForEach(p => {

                if (!this.replayGames)
                    return;

                Debug.WriteLine($"Replay score: {p.GetScorable().getFinalScore()}");

                var game = new Game(GameConfiguration.GetGameConfiguration());
                var ai = new T();
                var copyNetwork = p.GetNetwork().copyNetwork();
                ai.initializedNetwork(copyNetwork);


                this.startDrawGame(game);
                ai.playGame(game, 100, () => this.redrawGame());


                Task.Run(async delegate
                {
                    await Task.Delay(500);
                }).Wait();

            });

            Debug.WriteLine("Finished Replay");
        }

        private void startDrawGame(Game game)
        {
            MainWindow.Instance.Dispatcher.Invoke(() =>
            {
                drawer.displayGame(game);
            });
        }

        private void redrawGame()
        {
            MainWindow.Instance.Dispatcher.Invoke(() =>
            {
                drawer.draw();
            });
        }

        private void runGames(List<T> networks)
        {
            var networksByGames = networks
                .Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / config.parallelGames)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();

            networksByGames.AsParallel()
                .ForAll(netList => {
                    netList.ForEach( network => {
                        var game = new Game(GameConfiguration.GetGameConfiguration());
                        network.playGame(game);
                        network.GetScorable().fixFinalScore(game);
                    });
                });
        }


        private List<T> generateGeneration(int quantity)
        {
            var quantityByThread = quantity / bestParents.Count;

            var createdNetworks = bestParents
                .AsParallel()
                .Select(n => {
                    return createScorablePack(quantityByThread, n); })
                .SelectMany(n => n)
                .ToList();

            return createdNetworks;

        }

        private IEnumerable<T> createScorablePack(int quantity,T parentNetwork)
        {
            Random r = new Random();

            for(int x = 0; x<Math.Ceiling(quantity/2.0); x++)
            {
                var toMute = parentNetwork.GetNetwork().copyNetwork();
                toMute.applyMutation(config.mutationChance);
               
                yield return createScorable(toMute);

                var randomPartner = bestParents[r.Next(0, bestParents.Count)];
                var toCross = parentNetwork.GetNetwork().copyNetwork();
                
                //toCross.applyCrossOver(randomPartner.GetNetwork(), config.crossOverChance);
                toCross.applyCrossOver(randomPartner.GetNetwork(), r.NextDouble());
                toCross.applyMutation(config.mutationChance);

                yield return createScorable(toCross);
            }
        }

        private T createScorable(INetwork net)
        {
            var createdInstance = Activator.CreateInstance(typeof(T), net) as T;

            if (createdInstance == null) throw new Exception("Failed to instantiate network");

            return createdInstance;
        }

    }
}
