﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnakeAi.Models.Ai
{
    public interface ISavableNetwork
    {
        string networkToString();

        INetwork stringToNetwork(string content);

    }
}
