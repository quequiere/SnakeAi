﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace SnakeAi.Models.Ai
{
    public class DisplayableData : INotifyPropertyChanged
    {
        private int generation = 0;
        private int networks = 0;
        private double bestScoreNetwork = 0;

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public int currentGeneration 
        {
            get {
                return this.generation;
            }
            set {
                NotifyPropertyChanged();
                this.generation = value;
            }
        }

        public int totalNetworkCreated
        {
            get
            {
                return this.networks;
            }
            set
            {
                NotifyPropertyChanged();
                this.networks = value;
            }
        }

        public double bestScore
        {
            get
            {
                return this.bestScoreNetwork;
            }
            set
            {
                NotifyPropertyChanged();
                this.bestScoreNetwork = value;
            }
        }


    }
}
