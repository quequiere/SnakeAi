﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnakeAi.Models.SnakeModels.Helpers
{
    public class GameConfiguration
    {
        public int gameSize { get; private set; }
        public int maxUselessMove { get; private set; }

        public GameConfiguration(int gameSize, int maxUselessMove)
        {
            this.gameSize = gameSize;
            this.maxUselessMove = maxUselessMove;
        }


        public static GameConfiguration GetGameConfiguration()
        {
            return new GameConfiguration(10, 100);
        }
    }
}
