﻿using SnakeAi.Models.SnakeModels.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace SnakeAi.Models.SnakeModels
{
    public class Game
    {
        public GameConfiguration Config { get; }

        public ScoreCalulator Score { get; }
        public PhysicHelper physicHelper { get; }


        public Snake snake { get; }
        public Apple apple { get; set; }

        public bool gameIsLost { get; private set; } = false;

        public Game(GameConfiguration gameConfig)
        {
            this.Config = gameConfig;

            this.physicHelper = new PhysicHelper(this);

            this.apple = new LocalisationInitializer<Apple>(this.Config).instantiateWithRandomLocation();

            do
            {
                this.snake = new LocalisationInitializer<Snake>(this.Config).instantiateWithRandomLocation();
            } while (physicHelper.isCollideApple(this.snake.getLocation()));

         

            this.Score = new ScoreCalulator();
        }

        public void fireAction(Direction direction)
        {

            var futurLocation = DirectionHelper.applyDirection(direction, this.snake.getLocation());

            if (gameIsLost)
            {
                throw new Exception("Try to do action but game is finished");
            }
            else if (this.physicHelper.isCollideWall(futurLocation) || this.physicHelper.isCollideSnake(futurLocation))
            {
                gameIsLost = true;
            }
            else if (this.snake.getUselessMove() > this.Config.maxUselessMove)
            {
                gameIsLost = true;
            }
            else if(this.physicHelper.isCollideApple(futurLocation))
            {
                this.Score.eatApple();
                this.snake.resetUselessMove();
                this.snake.moveDirectionAndGrow(direction);

                do
                {
                    this.apple = new LocalisationInitializer<Apple>(this.Config).instantiateWithRandomLocation();
                }
                while (physicHelper.isCollideSnake(this.apple.getLocation()));

                this.Score.updateLifetime();
            }
            else
            {
                this.snake.incrementUselessMove();
                this.snake.moveDirection(direction);

                this.Score.updateLifetime();
            }

        }
    }
}
