﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnakeAi.Models.SnakeModels
{
    public class BodyPart : ILocalizable
    {
        private int xPos { get;  set; }
        private int yPos { get;  set; }

        public BodyPart(int x, int y)
        {
            this.xPos = x;
            this.yPos = y;
        }

        public (int xPos, int yPos) getLocation()
        {
            return (xPos, yPos);
        }

        public void setLocation(int xPos, int yPos)
        {
            this.xPos = xPos;
            this.yPos = yPos;
        }
    }
}
