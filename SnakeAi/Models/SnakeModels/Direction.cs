﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace SnakeAi.Models.SnakeModels
{
    public enum Direction
    {
        north,
        south,
        est,
        west
    }

    public class DirectionHelper
    {
        public static (int xpos, int ypos) directionToCoordinates(Direction direction)
        {
            switch (direction)
            {
                case Direction.north:
                    return (0, 1);
                case Direction.south:
                    return (0, -1);
                case Direction.west:
                    return (-1, 0);
                case Direction.est:
                    return (1, 0);
                default:
                    throw new Exception("Unknow direction");
            }
        }

        public static (int xpos, int ypos) applyDirection(Direction direction, (int, int) coordinates)
        {
            var newPos = directionToCoordinates(direction);
            return (coordinates.Item1 + newPos.xpos, coordinates.Item2 + newPos.ypos);
        }

        public static Direction? keyToDirection(Key key)
        {
            switch (key)
            {
                case Key.Up:
                    return Direction.north;
                case Key.Down:
                    return Direction.south;
                case Key.Right:
                    return Direction.est;
                case Key.Left:
                    return Direction.west;
                default:
                    return null;
            }
        }

        public static Direction getOpposite(Direction direction)
        {
            switch (direction)
            {
                case Direction.north:
                    return Direction.south;
                case Direction.south:
                    return Direction.north;
                case Direction.west:
                    return Direction.est;
                case Direction.est:
                    return Direction.west;
                default:
                    throw new Exception("Unknow direction");
            }
        }

    }
}
