﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnakeAi.Models.SnakeModels.Helpers
{
    public class LocalisationInitializer<T> where T : ILocalizable, new()
    {
        private GameConfiguration gameConfiguration {get;}

        public LocalisationInitializer(GameConfiguration gameConfiguration)
        {
            this.gameConfiguration = gameConfiguration;
        }

        public T instantiateWithRandomLocation()
        {
            var loc = generateLocation();
            var instance = new T();
            instance.setLocation(loc.xpos,loc.ypos);

            return instance;
        }

        public (int xpos,int ypos) generateLocation()
        {
            var random = new Random();
            var xLoc = random.Next(0, gameConfiguration.gameSize);
            var yLoc = random.Next(0, gameConfiguration.gameSize);
            return (xLoc, yLoc);
        }
    }
}
