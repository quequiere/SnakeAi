﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnakeAi.Models.SnakeModels.Helpers
{
    public class ScoreCalulator 
    {

        public int appleEat { get; private set; }
        public int lifeTime { get; private set; }
    
        public void eatApple()
        {
            appleEat++;
        }

        public void updateLifetime()
        {
            lifeTime++;
        }
    }
}
