﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace SnakeAi.Models.SnakeModels.Helpers
{
    public class PhysicHelper
    {
        private Game game { get; }

        public PhysicHelper (Game game)
        {
            this.game = game;
        }

        public bool isCollideWall((int x,int y) location)
        {
            return (location.x > game.Config.gameSize - 1 || location.y > game.Config.gameSize - 1 || location.x < 0 || location.y < 0);
        }

        public bool isCollideApple((int x, int y) location)
        {
            return (game.apple.getLocation().xPos == location.x && game.apple.getLocation().yPos == location.y);
        }

        public bool isCollideSnake((int x, int y) location)
        {
            return this.game.snake.GetBodies().Any(part => part.getLocation().xPos == location.x && part.getLocation().yPos == location.y);
        }
    }
}
