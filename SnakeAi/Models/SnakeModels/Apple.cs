﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnakeAi.Models.SnakeModels
{
    public class Apple : ILocalizable
    {
        private BodyPart BodyPart { get; set; }

        public (int xPos, int yPos) getLocation()
        {
            return BodyPart.getLocation();
        }

        public void setLocation(int xPos, int yPos)
        {
            if (BodyPart == null)
                BodyPart = new BodyPart(xPos, yPos);
            else
                BodyPart.setLocation(xPos, yPos);
        }
    }
}
