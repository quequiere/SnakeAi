﻿using SnakeAi.Models.SnakeModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SnakeAi.Models.SnakeModels
{
    public class Snake : ILocalizable, IBodyMember, IMovable
    {
        private List<BodyPart> bodyParts { get; } = new List<BodyPart>();

        private double useLessMove { get; set; }

        public List<BodyPart> GetBodies()
        {
            return bodyParts;
        }

        public (int xPos, int yPos) getLocation()
        {
            var lastBodyPartLoc = bodyParts[bodyParts.Count - 1].getLocation();
            return (lastBodyPartLoc.xPos, lastBodyPartLoc.yPos);
        }

        public void moveDirection(Direction direction)
        {
            this.doMove(direction);
        }

        public void moveDirectionAndGrow(Direction direction)
        {
            this.doMove(direction, false);
        }

        private void doMove(Direction direction, bool removeTail = true)
        {
            var newLoc = DirectionHelper.applyDirection(direction, this.getLocation());
            bodyParts.Add(new BodyPart(newLoc.xpos, newLoc.ypos));

            if(removeTail)
                bodyParts.RemoveAt(0);
        }


        public void setLocation(int xPos, int yPos)
        {
            bodyParts.Clear();
            bodyParts.Add(new BodyPart(xPos, yPos));
        }

        public void incrementUselessMove()
        {
            useLessMove++;
        }

        public double getUselessMove()
        {
            return useLessMove;
        }

        public void resetUselessMove()
        {
            useLessMove = 0;
        }
    }
}
