﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnakeAi.Models.SnakeModels
{
    public interface ILocalizable
    {
        (int xPos, int yPos) getLocation();
        void setLocation(int xPos, int yPos);
    }
}
