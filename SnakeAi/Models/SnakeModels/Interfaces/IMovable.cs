﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnakeAi.Models.SnakeModels.Interfaces
{
    public interface IMovable
    {
        void moveDirection(Direction direction);

        void incrementUselessMove();

        double getUselessMove();

        void resetUselessMove();
    }
}
