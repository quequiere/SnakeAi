﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SnakeAi.Models.SnakeModels.Interfaces
{
    public interface IBodyMember
    {
        List<BodyPart> GetBodies();
    }
}
