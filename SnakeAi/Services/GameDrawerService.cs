﻿using SnakeAi.Models.SnakeModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SnakeAi.Services
{
    public class GameDrawerService
    {
  
        public Game? CurrentGame { get; private set; }
        private Grid grid { get; set; }


        public GameDrawerService(Grid grid)
        {
            if (grid == null)
                throw new Exception("Grid can't be null !");

            this.grid = grid;
        }

        public void displayGame(Game game)
        {
            this.CurrentGame = game;
            this.draw();
        }

        private void clear()
        {
            if (CurrentGame == null) throw new Exception("Error, can't draw empty game !");

            grid.RowDefinitions.Clear();
            grid.ColumnDefinitions.Clear();
            grid.Children.Clear();

            grid.Children.RemoveRange(0, grid.Children.Count - 1);


            for (int x = 0; x < CurrentGame.Config.gameSize; x++)
            {
                grid.RowDefinitions.Add(new RowDefinition());
                grid.ColumnDefinitions.Add(new ColumnDefinition());
            }
        }

        public void draw()
        {
            this.clear(); ;
            this.drawSnake();
            this.drawApple();
        }

        private void drawSnake()
        {
            if (CurrentGame == null) throw new Exception("Error, can't draw empty game !");
            CurrentGame.snake.GetBodies().ForEach(body => drawLocation(body));
        }

        private void drawApple()
        {
            if (CurrentGame == null) throw new Exception("Error, can't draw empty game !");
            drawLocation(CurrentGame.apple);
        }

        private void drawLocation(ILocalizable locObject)
        {
            Rectangle r = new Rectangle();


            var color = Colors.Purple;

            if(locObject is BodyPart)
            {
                color = Colors.White;
            }
            else if(locObject is Apple)
            {
                color = Colors.Red;
            }


            r.Fill = new SolidColorBrush(color);

            var coordinates = getGridCoordinates(locObject);

            Grid.SetColumn(r, coordinates.x);
            Grid.SetRow(r, coordinates.y);

            grid.Children.Add(r);
        }


        private (int x, int y) getGridCoordinates(ILocalizable locObject)
        {
            if (CurrentGame == null) throw new Exception("Error, can't draw empty game !");
            var x = locObject.getLocation().xPos;
            var y = (CurrentGame.Config.gameSize - 1) - locObject.getLocation().yPos;
            return (x, y);
        }

    }
}
