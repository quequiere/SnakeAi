﻿using ArtificialNeuralNetwork;
using ArtificialNeuralNetwork.ActivationFunctions;
using ArtificialNeuralNetwork.Factories;
using ArtificialNeuralNetwork.WeightInitializer;
using SnakeAi.AITypes.jobeland;
using SnakeAi.Models.Ai;
using SnakeAi.Models.SnakeModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Newtonsoft.Json;
using ArtificialNeuralNetwork.Genes;
using SnakeAi.Models.SnakeModels.Helpers;

namespace SnakeAi.AITypes
{
    public class JLAIManager : ScorableNetworkBase, ISavableNetwork
    {
        private JLAiNetwork JLAiNetwork { get; set; }


        public JLAIManager()
        {
            this.initializedRandomNetwork();
        }


        public JLAIManager(INetwork network)
        {
            this.initializedNetwork(network);
        }

        public override INetwork GetNetwork()
        {
            if (JLAiNetwork == null) throw new Exception("Neural network not initialized !");
            return JLAiNetwork;
        }

        public override IScorable GetScorable()
        {
            if (JLAiNetwork == null) throw new Exception("Neural network not initialized !");
            return JLAiNetwork;
        }

        public override INetwork initializedNetwork(INetwork network)
        {
            JLAiNetwork = network as JLAiNetwork;
            return JLAiNetwork;
        }

        public override INetwork initializedRandomNetwork()
        {
            JLAiNetwork = new JLAiNetwork(createCustomNetwork());
            return JLAiNetwork;
        }


        public string networkToString()
        {
            return JsonConvert.SerializeObject(this.JLAiNetwork.NeuralNetwork.GetGenes());
        }

        public INetwork stringToNetwork(string content)
        {
            var genes = JsonConvert.DeserializeObject<NeuralNetworkGene>(content);
            return new JLAiNetwork(getNeuralFactoryInstance().Create(genes));
    
         }

        private static INeuralNetwork createCustomNetwork()
        {
            if(JLConfiguration.fullVisionMod)
            {
                var gameSize = GameConfiguration.GetGameConfiguration().gameSize;

                var numInputs = (gameSize * gameSize) + 2 ;
                var numOutputs = 4;
                var numHiddenLayers = 2;
                var numNeuronsInHiddenLayer = 20;
                return getNeuralFactoryInstance().Create(numInputs, numOutputs, numHiddenLayers, numNeuronsInHiddenLayer);
            }
            else
            {
                var numInputs = 24;
                var numOutputs = 4;
                var numHiddenLayers = 2;
                var numNeuronsInHiddenLayer = 18;
                return getNeuralFactoryInstance().Create(numInputs, numOutputs, numHiddenLayers, numNeuronsInHiddenLayer);
            }
      
        }


        public static NeuralNetworkFactory getNeuralFactoryInstance()
        {
            var somaFactory = SomaFactory.GetInstance(new SimpleSummation());
            var axonFactory = AxonFactory.GetInstance(new RectifiedLinearActivationFunction());
            var randomInit = new RandomWeightInitializer(new Random());
            var hiddenSynapseFactory = SynapseFactory.GetInstance(randomInit, axonFactory);
            var ioSynapseFactory = SynapseFactory.GetInstance(new ConstantWeightInitializer(1.0), axonFactory);
            var neuroFactory = NeuronFactory.GetInstance();
            var biasInitializer = randomInit;
            return NeuralNetworkFactory.GetInstance(somaFactory, axonFactory, hiddenSynapseFactory, ioSynapseFactory, biasInitializer, neuroFactory);

        }

    }


}
