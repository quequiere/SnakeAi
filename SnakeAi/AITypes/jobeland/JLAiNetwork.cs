﻿using ArtificialNeuralNetwork;
using SnakeAi.Models.Ai;
using SnakeAi.Models.SnakeModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SnakeAi.Models.Ai.helpers;
using SnakeAi.Models.Ai.Visions;
using System.Diagnostics.CodeAnalysis;

namespace SnakeAi.AITypes.jobeland
{
    public class JLAiNetwork : INetwork, IScorable
    {
        public INeuralNetwork NeuralNetwork { get; }

        private double score { get; set; } = -1;

        private IVision vision { get; }

        public JLAiNetwork(INeuralNetwork neuralNetwork)
        {
            this.NeuralNetwork = neuralNetwork;
            if (JLConfiguration.fullVisionMod)
            {
                this.vision = new FullVision();

            }
            else
            {
                this.vision = new BasicVision();

            }
        }



        public void applyCrossOver(INetwork partner, double chance)
        {
            var partnerReal = partner as JLAiNetwork;

            var otherNeurones = partnerReal.getAllNeurons();

            this.getAllNeurons()
                .Select((neuron, index) => (neuron, index))
                .ToList()
                .ForEach(n => {
                    n.neuron.Soma.Dendrites.Select((dendrite, identrite) => (dendrite, identrite)).ToList().ForEach(s =>
                    {
                        s.dendrite.Weight = AiTools.tryMixRandom(chance, s.dendrite.Weight, otherNeurones[n.index].Soma.Dendrites[s.identrite].Weight);
                    });
                });

        }

        public void applyMutation(double chance)
        {
            this.getAllNeurons().ForEach(n => {
                n.Soma.Dendrites.ToList().ForEach(synapse =>
                {
                    synapse.Weight = AiTools.tryChangeRandom(chance, synapse.Weight);
                });
            });


        }

        public double[] computeNetwork(double[] inputs)
        {
            this.NeuralNetwork.SetInputs(inputs);
            this.NeuralNetwork.Process();
            return this.NeuralNetwork.GetOutputs();
        }

        public INetwork copyNetwork()
        {
            return new JLAiNetwork(copyNetworkJLai());
        }

        public double[] generateVision(Game game)
        {
            return vision.generateVision(game);
        }


        public double getFinalScore()
        {
            return this.score;
        }

        public void fixFinalScore(Game game)
        {
            if(this.score==-1)
            {
                var calculated = (game.Score.lifeTime * game.Score.lifeTime) * Math.Pow(3, game.Score.appleEat);
                this.score = calculated;
            }
            else
            {
                throw new Exception("Score already fixed");
            }
          
        }





        private INeuralNetwork copyNetworkJLai()
        {
            return JLAIManager.getNeuralFactoryInstance().Create(this.NeuralNetwork.GetGenes());
        }

        private List<INeuron> getAllNeurons()
        {
            var list = new List<INeuron>();

            //This one has two layer, one is connected to input L0, the other one to L1
            list.AddRange(this.NeuralNetwork.HiddenLayers.SelectMany(layer => layer.NeuronsInLayer).ToList());
            list.AddRange(this.NeuralNetwork.OutputLayer.NeuronsInLayer.ToList());
            return list;
        }


        public int CompareTo([AllowNull] IScorable other)
        {
            return this.getFinalScore().CompareTo(other.getFinalScore());
        }
    }
}
