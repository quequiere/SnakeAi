﻿using Microsoft.Win32;
using SnakeAi.AITypes;
using SnakeAi.Models.Ai;
using SnakeAi.Models.SnakeModels;
using SnakeAi.Models.SnakeModels.Helpers;
using SnakeAi.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SnakeAi
{

    public partial class MainWindow : Window
    {
        public static GameDrawerService GameDrawerService;
        public static MainWindow Instance;

        private static AiManager<JLAIManager> aiManager { get; set; }
        private static DisplayableData dataContextAI { get; set; }


        public MainWindow()
        {
            InitializeComponent();
            Instance = this;
            GameDrawerService = new GameDrawerService(displayedGame);

        }

        public void startPlayerAi()
        {
            Debug.WriteLine("Ask ai start");

            if (aiManager == null) this.initializeAI();

            dataContextAI = aiManager.displayableData;
            aiManager.startAi();
            Debug.WriteLine("Ask started !");
        }

        public void initializeAI()
        {
            aiManager = new AiManager<JLAIManager>(GameDrawerService);
        }

        public void startPlayerGame()
        {
            var game = new Game(GameConfiguration.GetGameConfiguration());
            GameDrawerService.displayGame(game);
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            var direction = DirectionHelper.keyToDirection(e.Key);
            if (direction.HasValue && GameDrawerService.CurrentGame != null)
            {
                GameDrawerService.CurrentGame.fireAction(direction.Value);
                if(GameDrawerService.CurrentGame.gameIsLost)
                {
                    LoosePopup popup = new LoosePopup();
                    popup.ShowDialog();
                }

                GameDrawerService.draw();


            }
               

        }

        private void Button_StartAI(object sender, RoutedEventArgs e)
        {
          
            this.startPlayerAi();

            this.label_generationnb.DataContext = aiManager.displayableData;
            this.label_numbertrainedNetwork.DataContext = aiManager.displayableData;
            this.label_BestScore.DataContext = aiManager.displayableData;
            

            this.labelReplay.IsEnabled = true;
            this.buttonTogglePlay.IsEnabled = true;
            this.saveButton.IsEnabled = true;
            this.loadingButton.IsEnabled = false;
            this.buttonStopAI.IsEnabled = true;
            this.replay.IsEnabled = false;
            
            this.buttonStartAI.IsEnabled = false;
        }

        private void Button_ToggleReplay(object sender, RoutedEventArgs e)
        {
            aiManager.replayGames = !aiManager.replayGames;
            this.labelReplay.Content = aiManager.replayGames?"YES":"NO";
        }

        private void Button_SaveNetwork(object sender, RoutedEventArgs e)
        {
            if(aiManager != null)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Json file (*.json)|*.json";
                saveFileDialog.InitialDirectory = @"c:\";
                saveFileDialog.FileOk += FileSelectedForSave;
                saveFileDialog.ShowDialog();
            }
        }

        private void FileSelectedForSave(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var dialog = sender as SaveFileDialog;
            if (!aiManager.saveNetwork(dialog.FileName))
            {
                MessageBox.Show("Failed to save file ! Maybe this network can't support save system.", "Fail save");
            }
        }

        private void Button_LoadNetwork(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dia = new OpenFileDialog();
            dia.Filter = "Json file (*.json)|*.json";
            dia.InitialDirectory = @"c:\";
            dia.FileOk += FileSelectedToLoad;
            dia.ShowDialog();


        }

        private void FileSelectedToLoad(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var dialog = sender as OpenFileDialog;
            this.initializeAI();
            if(!aiManager.loadNetwork(dialog.FileName))
            {
                MessageBox.Show("Failed to load neural network.");

            }
            else
                this.replay.IsEnabled = true;


        }

        private void Button_StopAI(object sender, RoutedEventArgs e)
        {
            this.buttonStopAI.IsEnabled = false;
            this.replay.IsEnabled = true;
            this.buttonStartAI.IsEnabled = true;

            aiManager.stopAI();
        }

        private void Button_PlayParents(object sender, RoutedEventArgs e)
        {
            Task.Run(delegate {
                aiManager.replayOnDemand();

            });
        }
    }
}
